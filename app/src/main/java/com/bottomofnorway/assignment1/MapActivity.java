package com.bottomofnorway.assignment1;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;


public class MapActivity extends FragmentActivity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationListener {

    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 20;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    LocationRequest mLocationRequest;
    LocationClient mLocationClient;
    boolean mUpdatesRequested;
    Location mCurrentLocation;
    GoogleMap map;
    Marker mark;
    String temp = "";
    Address bestMatch = null;

    LinearLayout layout;
    TextView textView;
    Button saveButton;
    boolean buttonAdded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        //find layout and textview
        textView = (TextView) findViewById(R.id.tv);
        layout = (LinearLayout) findViewById(R.id.layout);

        saveButton = new Button(this);

        //create new locationRequest
        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        mLocationClient = new LocationClient(this,this,this);
        mUpdatesRequested = false;
        mLocationClient.connect();

        //acquiring the map
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();


        saveButton.setText("Save location");
        //onClick listener for saveButton
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveLoc();
            }
        });

    }

    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;

        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    /*
    * Handle results returned to the FragmentActivity
    * by Google Play services
    */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    /*
                     * Try the request again
                     */
                        break;
                }
        }
    }


    /**
     * Checks if google services is available
     * @return true if google services is available
     *         false if its not
     */
    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates", "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason.
            // resultCode holds the error code.
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode,this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                //errorFragment.show(getSupportFragmentManager(),"Location Updates");

            }
        }
        return false;
    }

    /**
     * Override method that runs when connected
     * Finds the last location and centers the map on the location
     * @param dataBundle
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        // Display the connection status

        if (!mUpdatesRequested) {
            mLocationClient.requestLocationUpdates(mLocationRequest, this);
            mUpdatesRequested = true;
        }

        mCurrentLocation = mLocationClient.getLastLocation();

        double lat = mCurrentLocation.getLatitude();
        double lon = mCurrentLocation.getLongitude();

        mark = map.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));

        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(lat, lon));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        CameraUpdate centerZoom = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 12);

        map.animateCamera(centerZoom);

        textView.append("Last location: \n");
        textView.append("Lat: " + String.valueOf(mCurrentLocation.getLatitude()) + "N");
        textView.append("\nLon: " + String.valueOf(mCurrentLocation.getLongitude()) + "E\n");
        textView.append("Wait until new location is found..");


        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
    }

    /**
     * runs when disconnected
     */
    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * no resolution is available
             */
        }
    }

    /**
     * connects to the location client
     */
    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        mLocationClient.connect();
    }

    /**
     * Disconnects from the location client
     */
    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
    }

    /**
     * Define the callback method that receives location updates
     * Shows the user the new location on the map, and makes the
     * saveButton available. Starts an async method to retrieve
     * temperature at the location from openweathermap.org
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        if(!buttonAdded){
            layout.addView(saveButton, 0);
            buttonAdded = true;
        }


        //sets mCurrentLocation to the new location
        mCurrentLocation = location;
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

        Geocoder geocoder = new Geocoder(this);

        try {
            List<Address> matches = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            bestMatch = (matches.isEmpty() ? null : matches.get(0));
            Thread.sleep(500);
        }catch (Exception e){

        }

        //async method that retrieves weather data from openweathermap.org
        try {
            temp = new RetrieveWeather().execute("http://api.openweathermap.org/data/2.5/weather?lat=\" + location.getLatitude() + \"&lon=\" + location.getLongitude() + \"&units=metric&APPID=1481b05af50541b7e060c839be60a1e6").get();

        } catch (Exception e) {

        }


        textView.setText("");
        if(bestMatch != null) {
            textView.append("\nLocality: " + bestMatch.getLocality());
        }

        textView.append("\nLat: " + String.valueOf(location.getLatitude()));
        textView.append("\nLng: " + String.valueOf(location.getLongitude()));
        textView.append("\nAlt: " + String.valueOf(location.getAltitude() + "M"));
        textView.append("\nTemp: " + temp + "C");
        if(location.getAltitude() == 0.0) {
            textView.append("\nIf you want an altitude,\nmake sure enough GPS satellites see you.");
        }



    }

    /**
     * Shows this activity
     * @param context
     */
    static void show(Context context){
        final Intent intent = new Intent(context, MapActivity.class);
        context.startActivity(intent);
    }

    /**
     * Async method that retrieves weather data from openweathermaps.org
     */
    private class RetrieveWeather extends AsyncTask<String, String, String> {
        String responseString = "";
        @Override
        protected String doInBackground(String... strings) {
            int count;
            try {

                URL url = new URL (strings[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                InputStream input = new BufferedInputStream(url.openStream(), 10*1024);
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+"/weather.txt");
                byte data[] = new byte[1024];
                long total = 0;
                while((count = input.read(data)) != -1){
                    total += count;
                    output.write(data, 0, count);
                }
                String jsonData = new String(data, "UTF-8");
                output.flush();
                output.close();
                input.close();

                //parsing the data
                JSONObject obj = new JSONObject(jsonData);
                JSONObject sys = obj.getJSONObject("sys");
                JSONObject main = obj.getJSONObject("main");

                String temp = main.getString("temp");

                responseString = temp;



            }catch (Exception e){
                responseString = "Error" + e;
            }
            return responseString;
        }

        protected void onProgressUpdate(Integer... progress) {

        }
    }

    /**
     * Method that saves the current location to the database.
     */
    public void saveLoc(){
        AdbHandler adbHandler = new AdbHandler(this, null, null, 1);
        SavedLocation savedLocation = new SavedLocation(mCurrentLocation.getLatitude(),
                mCurrentLocation.getLongitude(), mCurrentLocation.getAltitude(),
                Double.parseDouble(temp), mCurrentLocation.getTime(), bestMatch.getLocality()
                );
        adbHandler.addStoredLocation(savedLocation);

        saveButton.setText("Location saved");
    }
}

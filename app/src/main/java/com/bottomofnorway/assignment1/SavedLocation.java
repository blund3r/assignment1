package com.bottomofnorway.assignment1;

/**
 * Created by Pål S on 15.09.2014.
 * This class stores id, latitude, longitude, altitude, temperature, time and
 * locality for a location
 */
public class SavedLocation {
    private int _id;
    private double _lat;
    private double _lng;
    private double _alt;
    private double _temp;
    private long _time;
    private String _locality;

    public SavedLocation(){

    }

    public SavedLocation(int id, double lat, double lng, double alt, double temp, long time, String locality){
        this._id = id;
        this._lat = lat;
        this._lng = lng;
        this._alt = alt;
        this._temp = temp;
        this._time = time;
        this._locality = locality;
    }

    public SavedLocation(double lat, double lng, double alt, double temp, long time, String locality){
        this._lat = lat;
        this._lng = lng;
        this._alt = alt;
        this._temp = temp;
        this._time = time;
        this._locality = locality;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public double get_lat() {
        return _lat;
    }

    public void set_lat(double _lat) {
        this._lat = _lat;
    }

    public double get_lng() {
        return _lng;
    }

    public void set_lng(double _lng) {
        this._lng = _lng;
    }

    public double get_alt() {
        return _alt;
    }

    public void set_alt(double _alt) {
        this._alt = _alt;
    }

    public double get_temp() {
        return _temp;
    }

    public void set_temp(double _temp) {
        this._temp = _temp;
    }

    public long get_time() {
        return _time;
    }

    public void set_time(long _time) {
        this._time = _time;
    }

    public String get_locality() {
        return _locality;
    }

    public void set_locality(String _locality) {
        this._locality = _locality;
    }
}

package com.bottomofnorway.assignment1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class HistoryActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        LinearLayout buttonContainer = (LinearLayout) findViewById(R.id.buttonContainer);


        AdbHandler adbHandler = new AdbHandler(this, null, null, 1);

        SavedLocation[] sLocations = adbHandler.returnSavedLocations();
        Button button[] = new Button[sLocations.length];


        for(int i = 0; i < sLocations.length; i++){
            if(sLocations[i] != null) {
                button[i] = new Button(this);
                button[i].append(sLocations[i].get_locality() + "\n");
                button[i].append("Coords: " + sLocations[i].get_lat() + "N ");
                button[i].append(sLocations[i].get_lng() + "E\n");
                button[i].append("Altitude: " + sLocations[i].get_alt() + "m\n");
                button[i].append("temperature: " + sLocations[i].get_temp() + "C\n");
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date(sLocations[i].get_time());
                String formatted = format.format(date);
                button[i].append(formatted);

            }
        }
        for(int i = 0; i < button.length; i++) {
            if(button[i] != null) {
                buttonContainer.addView(button[i]);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

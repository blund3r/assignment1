package com.bottomofnorway.assignment1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Database-handler for the database containing the saved locations
 * Created by Pål S on 15.09.2014.
 */
public class AdbHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "savedLocationDB.db";
    private static final String TABLE_SAVEDLOCATIONS = "savedLocations";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LAT = "latitude";
    public static final String COLUMN_LNG = "longitude";
    public static final String COLUMN_ALT = "altitude";
    public static final String COLUMN_TEMP = "temperature";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_LOCALITY = "locality";

    public AdbHandler(Context context, String name,
                       SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_SAVEDLOCATIONS_TABLE = "CREATE TABLE " +
                TABLE_SAVEDLOCATIONS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_LAT
                + " FLOAT," + COLUMN_LNG + " FLOAT," + COLUMN_ALT
                + " FLOAT," + COLUMN_TEMP + " FLOAT," + COLUMN_TIME
                + " LONG," + COLUMN_LOCALITY + " TEXT" + ")";
        db.execSQL(CREATE_SAVEDLOCATIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                          int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVEDLOCATIONS);
        onCreate(db);
    }

    /**
     * Adds a location to the SQLite db
     * @param savedLocation
     */
    public void addStoredLocation(SavedLocation savedLocation) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_LAT, savedLocation.get_lat());
        values.put(COLUMN_LNG, savedLocation.get_lng());
        values.put(COLUMN_ALT, savedLocation.get_alt());
        values.put(COLUMN_TEMP, savedLocation.get_temp());
        values.put(COLUMN_TIME, savedLocation.get_time());
        values.put(COLUMN_LOCALITY, savedLocation.get_locality());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_SAVEDLOCATIONS, null, values);
        db.close();
    }

    /**
     * Method that retrieves all saved locations from the db
     * @return SavedLocation[]
     */
    public SavedLocation[] returnSavedLocations() {
        String query = "Select * FROM " + TABLE_SAVEDLOCATIONS;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        String query2 = "SELECT Count(*) FROM " + TABLE_SAVEDLOCATIONS;
        SavedLocation savedLocations[] = new SavedLocation[30];

        int counter = 0;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SavedLocation savedLocation = new SavedLocation();
            savedLocation.set_id(Integer.parseInt(cursor.getString(0)));
            savedLocation.set_lat(Double.parseDouble(cursor.getString(1)));
            savedLocation.set_lng(Double.parseDouble(cursor.getString(2)));
            savedLocation.set_alt(Double.parseDouble(cursor.getString(3)));
            savedLocation.set_temp(Double.parseDouble(cursor.getString(4)));
            savedLocation.set_time(Long.parseLong(cursor.getString(5)));
            savedLocation.set_locality(cursor.getString(6));

            savedLocations[counter] = savedLocation;

            counter++;
            cursor.moveToNext();

        }
        db.close();
        return savedLocations;
    }
}
